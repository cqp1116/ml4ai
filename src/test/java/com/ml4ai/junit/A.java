package com.ml4ai.junit;

import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class A {

    @Test
    public void testReshape() {
        INDArray x = Nd4j.randn(4, 4);
        System.out.println(x.reshape(-1, 2));
    }

}
